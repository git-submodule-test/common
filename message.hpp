#pragma once

#include <ostream>
#include <string>

#include <spdlog/fmt/ostr.h>
#include <zmq.hpp>

class Message
{
  public:
    Message();
    Message(zmq::message_t const& msg);
    void greeting(std::string const& msg);
    void who(std::string const& msg);
    std::string format() const;
    zmq::message_t pack() const;
  private:
    std::string _greeting;
    std::string _who;
};

std::ostream& operator<<(std::ostream& os, Message const& c);
