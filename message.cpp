#include "message.hpp"

#include <cstdint>
#include <string>

#include <zmq.hpp>

Message::Message()
: _greeting("Hello")
, _who("World")
{}

Message::Message(zmq::message_t const& msg)
{
    std::uint8_t const* data = reinterpret_cast<std::uint8_t const*>(msg.data());

    std::uint16_t n = *reinterpret_cast<std::uint16_t const*>(data);
    data += 2;

    _greeting = std::string(reinterpret_cast<char const*>(data),
                            reinterpret_cast<char const*>(data) + n + 1);
    data += n;

    n = *reinterpret_cast<std::uint16_t const*>(data);
    data += 2;

    _who = std::string(reinterpret_cast<char const*>(data),
                       reinterpret_cast<char const*>(data) + n + 1);
}

void Message::greeting(std::string const& s)
{
    _greeting = s;
}

void Message::who(std::string const& s)
{
    _who = s;
}

std::string Message::format() const
{
    return _greeting + ", " + _who + "!";
}

zmq::message_t Message::pack() const
{
    std::uint16_t n = 2 + _greeting.size() + 2 + _who.size();
    std::vector<std::uint8_t> buffer(n);
    std::uint8_t* data = buffer.data();

    n = static_cast<std::uint16_t>(_greeting.size());
    std::memcpy(data, &n, 2);
    data += 2;

    std::memcpy(data, _greeting.data(), _greeting.size());
    data += _greeting.size();

    n = static_cast<std::uint16_t>(_who.size());
    std::memcpy(data, &n, 2);
    data += 2;

    std::memcpy(data, _who.data(), _who.size());
    data += _who.size();

    return zmq::message_t(buffer.data(), buffer.size());
}

std::ostream& operator<<(std::ostream& os, const Message& c)
{
    return os << c.format();
}
